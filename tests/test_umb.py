"""Tests for umb_messenger.umb."""
import json
import unittest
from unittest import mock

from tests import fakes
from umb_messenger import umb

CHECKOUT = fakes.get_fake_checkout(actual_attrs={'tree_name': 'fedora'})


class TestHandleMessage(unittest.TestCase):
    """Tests for umb.handle_message()."""

    @mock.patch('umb_messenger.message_data.get_ready_for_test_data')
    @mock.patch('stomp.Connection')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_ready_for_test(self, mock_connection, mock_ready):
        """Sanity check for ready_for_test messages.

        The behavior is same for both pre_ and post_test messages so we don't
        need to test both.
        """
        message_good = {'message1': 'good'}
        mock_ready.return_value = message_good
        umb.handle_message(CHECKOUT, 'pre_test')

        # Verify "good" message and topic is handled correctly
        mock_connection.return_value.send.assert_called_with(
            '/topic/VirtualTopic.eng.cki.ready_for_test', json.dumps(message_good)
        )

        mock_ready.return_value = {'reason': 'this went wrong'}
        with self.assertLogs(umb.LOGGER, 'INFO') as log:
            umb.handle_message(CHECKOUT, 'pre_test')
            self.assertIn('error', log.output[-1])
